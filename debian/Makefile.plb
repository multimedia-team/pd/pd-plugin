lib.name = plugin~
plugin~.class.sources = \
	plugin~.c \
	jload.c \
	jsearch.c \
	$(empty)

datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(wildcard *.md) \
	ChangeLog \
	$(empty)

datadirs = \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
